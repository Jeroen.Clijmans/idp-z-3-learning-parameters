Selected Publications
=====================

Awards
------
* Simon Marynissen, et al. **On the relationship between Approximation Fixpoint Theory and Justification Theory**, Distinguished Paper Award at IJCAI 2021
* Aerts, B., Vandevelde, S., & Vennekens, J. (2020). **Tackling the DMN challenges with cDMN: A tight integration of DMN and constraint reasoning**. In International Joint Conference on Rules and Reasoning. Best Paper Award at RuleML+RR 2020
* Denecker, et al. **Ultimate Well-Founded and Stable Semantics for Logic Programs with Aggregates.** ICLP 2001, 20-Year Test of Time Award at ICLP 2021
* Denecker, M. **Extending classical logic with inductive definitions.** ICCL2020. 20 Year Test of Time Award at ICLP 2020
* Dasseville et al. **Combining DMN and the Knowledge Base Paradigm for Flexible Decision Enactment**, Winner of RuleML 2016 Challenge

Core technology
---------------
* De Cat, B., Bogaerts, B., Bruynooghe, M., Janssens, G. and Denecker, M., 2018. **Predicate logic as a modeling language: the IDP system.** In Declarative Logic Programming: Theory, Systems, and Applications (pp. 279-323).

Interactive Consultant
----------------------

* Carbonnelle, P., Vennekens, J., Denecker, M., Bogaerts, B. (2020). **Interactive Configuration Problems in Observable Environments.** Presented at the International Conference on Principles of Knowledge Representation and Reasoning, Rhodes, Greece, 12 Sep 2020-18 Sep 2020.
* Carbonnelle, P., Bogaerts, B., Vennekens, J., Denecker, M. (2020). **Interactive Advisor for Lax Legislation and Observable Situations.** Presented at the Workshop on Models of Legal Reasoning, Rhodes, Greece, 12 Sep 2020-14 Sep 2020.
* Carbonnelle, P., Aerts, B., Deryck, M., Vennekens, J., Denecker, M. (2019). **An Interactive Consultant**. Presented at the BNAIC, Bruxelles, 06 Nov 2019-08 Nov 2019

Applications
------------

* Vandevelde, S., Callewaert, B., Vennekens, J. (2022). **Context-Aware Verification of DMN.** Proceedings of the 55th Hawaii International Conference on System Sciences, 5 Jan 2022.
* Deryck, M., Coppens, B.J., Comenda, N., Vennekens, J. (2021). **Logical Reasoning application with NLP interface to construct the Knowledge Base.** (736-738). Presented at the BNAIC2021, Esch-sur-Alzette, Luxembourg, 10 Nov 2021-12 Nov 2021
* Deryck, M., Vennekens, J., Comenda, N., Coppens, B. (2021). **Combining Logic and Natural Language Processing to Support Investment Management.** In: KR2021 proceedings. Presented at the 18th International Conference on Principles of Knowledge Representation and Reasoning, Hanoi, 03 Nov 202
* Vandevelde, S., Etikala, V., Vanthienen, J., Vennekens, J. (2021). **Leveraging the Power of IDP with the Flexibility of DMN: a Multifunctional API.** In: Proceedings of RuleML+RR 2021. Presented at the RuleML+RR 2021, Leuven, Belgium.
* Vandevelde, S., Vennekens, J. (2020). **A Multifunctional, Interactive DMN Decision Modelling Tool.** Presented at the BNAIC 2020, Virtual, 19 Nov 2020-20 Nov 2020.

