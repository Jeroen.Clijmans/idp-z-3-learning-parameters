Interactive Consultant
======================
The interactive consultant tool enables experts to encode their knowledge of a specific problem domain in FO(.).
From the resulting knowledge base, a web tool is automatically created that helps end users find solutions for specific problems within that knowledge domain.

Demo
----

The interactive consultant is available `online <https://interactive-consultant.IDP-Z3.be/>`_, offering the possibility to everyone to specify a knowledge base using the FO(.) language and experiment with the resulting online web tool.
To quickstart your experience, the interactive consultant comes with two demo knowledge bases, available under the 'File' header:

* the 'Polygon' knowledge base, which specifies some common knowledge on the number of sides, their length and the angles for some common polygonal shapes; and
* the 'Registration' knowledge base, which models the Belgian federal law on property registration and the associated registration taxes.

A short introductory video on the interactive consultant can be found below:

.. rst-class:: center
.. video:: https://interactive-consultant.IDP-Z3.be/assets/Interactive_Consultant.mp4
   :width: 800
   :height: 480

Our partners at Flanders Make have created a promo video featuring an application:

.. rst-class:: center
.. youtube:: kpPe0GRjifE
   :width: 800
   :height: 480

Here is a more in-depth view of the application above:

.. rst-class:: center
.. youtube:: hO8k_64b_Oc
   :width: 800
   :height: 480

