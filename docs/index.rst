.. IDP-Z3 documentation main file, created by
   sphinx-quickstart on Mon Jun 29 12:09:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IDP-Z3's documentation!
==================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction
   FO-dot
   IDP-Z3
   interactive_consultant
   code_reference
   summary
   genindex



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`
